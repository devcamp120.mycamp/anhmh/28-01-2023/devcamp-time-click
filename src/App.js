import { useState } from 'react';
import './App.css';

function App() {
  let newTime = new Date().toLocaleTimeString();
  const [ctime, setCtime] = useState(newTime);
  const UpdateTime = () => {
    newTime = new Date().toLocaleTimeString();
    setCtime(newTime);
  };
  
  return (
    <div>
     <center>
      <h1>List</h1>
      <h2>{ctime}</h2>
      <button onClick={UpdateTime}>Get Time</button>
     </center>
    </div>
  );
}

export default App;
